<?php

declare(strict_types=1);

namespace LyraDataCollector;

use function array_merge;
use function array_unique;
use function count;
use const SORT_REGULAR;

abstract class AbstractDataCollector implements DataCollectorInterface
{
    private static $data = [];

    final public static function push($item)
    {
        self::$data[static::getCollectionName()][] = $item;
    }

    final public static function merge(array $data)
    {
        self::$data[static::getCollectionName()] = array_merge(self::get(), $data);
    }

    final public static function get(bool $onlyUnique = false): array
    {
        $data = self::$data[static::getCollectionName()] ?? [];

        return ($onlyUnique)
            ? array_unique($data, SORT_REGULAR)
            : $data;
    }

    final public static function fetch(bool $onlyUnique = false): array
    {
        $data = self::get($onlyUnique);

        self::reset();

        return $data;
    }

    final public static function reset()
    {
        unset(self::$data[static::getCollectionName()]);
    }

    final public static function count(bool $onlyUnique = false): int
    {
        return count(self::get($onlyUnique));
    }

    final public static function isEmpty(): bool
    {
        return !self::count();
    }

    abstract protected static function getCollectionName(): string;
}
