<?php

declare(strict_types=1);

namespace LyraDataCollector;

interface DataCollectorInterface
{
    public static function push($item);

    public static function merge(array $data);

    public static function get(bool $onlyUnique = false): array;

    public static function fetch(bool $onlyUnique = false): array;

    public static function reset();

    public static function count(bool $onlyUnique = false): int;

    public static function isEmpty(): bool;
}
