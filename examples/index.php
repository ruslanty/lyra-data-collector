<?php

declare(strict_types=1);

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
date_default_timezone_set('UTC');

require __DIR__ . '/../vendor/autoload.php';

$people = [
    'Elizabeth Debicki',
    'Robert Pattinson',
    'Kenneth Branagh',
    'John Washington',
    'David Washington',
    'Aaron Taylor-Johnson',
    'Clémence Poésy',
    'Fiona Dourif',
];

$additionalNames = [
    'Michael',
    'Andrew',
    'Wes',
    'Himesh',
    'Andrew',
    'Martin',
    'Dimple',
];

$additionalSurnames = [
    'Caine',
    'Caine',
    'Howard',
    'Chatham',
    'Patel',
    'Donovan',
    'Kapadia',
];

foreach ($people as $person) {
    list($name, $surname) = explode(' ', $person);

    \LyraDataCollector\Examples\Collectors\FirstNameCollector::push($name);
    \LyraDataCollector\Examples\Collectors\LastNameCollector::push($surname);
}

\LyraDataCollector\Examples\Collectors\FirstNameCollector::merge($additionalNames);
\LyraDataCollector\Examples\Collectors\LastNameCollector::merge($additionalSurnames);

$nameCount = \LyraDataCollector\Examples\Collectors\LastNameCollector::count();
$names = \LyraDataCollector\Examples\Collectors\FirstNameCollector::get();
$names = implode(', ', $names);

$uniqueNameCount = \LyraDataCollector\Examples\Collectors\FirstNameCollector::count(true);
$uniqueNames = \LyraDataCollector\Examples\Collectors\FirstNameCollector::fetch(true);
$uniqueNames = implode(', ', $uniqueNames);

$surnameCount = \LyraDataCollector\Examples\Collectors\LastNameCollector::count();
$surnames = \LyraDataCollector\Examples\Collectors\LastNameCollector::get();
$surnames = implode(', ', $surnames);

$uniqueSurnameCount = \LyraDataCollector\Examples\Collectors\LastNameCollector::count(true);
$uniqueSurnames = \LyraDataCollector\Examples\Collectors\LastNameCollector::fetch(true);
$uniqueSurnames = implode(', ', $uniqueSurnames);

echo '<pre>';

echo "Names ({$nameCount}): ", $names, PHP_EOL;
echo "Unique names ({$uniqueNameCount}): ", $uniqueNames, PHP_EOL;

echo "Surnames ({$surnameCount}): ", $surnames, PHP_EOL;
echo "Unique surnames ({$uniqueSurnameCount}): ", $uniqueSurnames, PHP_EOL;
