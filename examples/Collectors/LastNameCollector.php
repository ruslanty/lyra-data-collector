<?php

declare(strict_types=1);

namespace LyraDataCollector\Examples\Collectors;

use LyraDataCollector\AbstractDataCollector;

final class LastNameCollector extends AbstractDataCollector
{
    protected static function getCollectionName(): string
    {
        return __CLASS__;
    }
}
