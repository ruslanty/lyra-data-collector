# Lyra Data Collector

Simple PHP library to collect your data across classes

#### Requirements:
+ PHP 7.0 or higher
+ Composer (for installation)

#### Features:
+ Collect custom data across classes
+ Extendable, custom collectors
+ Functional tests

#### Installation:
```shell script
composer require ruslanty/lyra-data-collector
```
