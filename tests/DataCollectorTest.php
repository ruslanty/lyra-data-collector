<?php

declare(strict_types=1);

namespace LyraDataCollector\Tests;

use LyraDataCollector\AbstractDataCollector;
use PHPUnit\Framework\TestCase;

class DataCollectorTest extends TestCase
{
    private $collector;

    public function setUp()
    {
        $this->collector = new class extends AbstractDataCollector {
            protected static function getCollectionName(): string
            {
                return 'test_collector';
            }
        };
    }

    public function testDataCollector()
    {
        $item = 'item';

        $data = [
            'bool' => true,
            'int' => 13,
            'float' => 1.3,
            'string' => 'string',
            'array' => [
                'bool' => true,
                'int' => 13,
                'float' => 1.3,
                'string' => 'string',
                'array' => [1, 2, 3],
                'object' => new \stdClass(),
                'null' => null,
            ],
            'object' => new \stdClass(),
            'null' => null,
        ];

        $this->collector::push($item);
        $this->assertArrayHasKey(0, $this->collector::get());

        $this->collector::merge($data);
        $this->assertArrayHasKey(0, $this->collector::get());
        $this->assertArrayHasKey('bool', $this->collector::get());
        $this->assertArrayHasKey('null', $this->collector::get());

        $this->assertFalse($this->collector::isEmpty());

        $this->assertCount($this->collector::count(), $this->collector::get());
        $this->assertCount($this->collector::count(true), $this->collector::get(true));

        $data = $this->collector::fetch();
        $this->assertArrayHasKey('bool', $data);
        $this->assertCount(8, $data);

        $this->assertCount(0, $this->collector::get());
        $this->assertCount($this->collector::count(), $this->collector::get());
        $this->assertTrue($this->collector::isEmpty());
    }
}
